### Description
* Create a web form that should allow the user to enter a url.

* The application should then make a request a given web page

* It should analyse the page�s content and return some information about it to the browser.

 	*	The <title\> of the page
 	* 	Number of links on the page that the user can click on
 	* 	Number of unique domains that these links go to
 	* 	Whether the page was served in a secure manner
 	* 	Was Google Analytics available and working on the page?


# Instructions

```npm install```

```npm start```

or

```npm install```

```gulp```

All the above instructions can be in a deploy_script.sh for server deployment.