"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const linkProcessor_1 = require("../controllers/linkProcessor");
const chai_1 = require("chai");
require("mocha");
describe('Test Questions', () => {
    let linkProcessor = new linkProcessor_1.LinkProcessor();
    let protocolData = { request: { uri: { protocol: 'http:' } } };
    let websiteData = '<!DOCTYPE html>\n<html lang="en">\n\t<head>\n\t<meta name="generator" content="Hugo 0.41-DEV" />\n <script src="https://yahoo.com"></script><title>Yahoo</title></head>\n<body>\n<a class="site-title" href="/">\n<a class="site-title" href="/test">\n</body>\n</html>';
    const result = linkProcessor.getAnswers(websiteData);
    describe('Website Title', () => {
        it('should return title of the website', () => {
            chai_1.expect(result.title).to.equal('Yahoo');
        });
    });
    describe('Number of All Clickable Links', () => {
        it('should return the number of website links', () => {
            chai_1.expect(result.allLinks).to.equal(2);
        });
    });
    describe('Number of Unique Domains', () => {
        it('should return the number Of unique domains', () => {
            chai_1.expect(result.uniqueDomains).to.equal(2);
        });
    });
    describe('Site Uses Google Anaytic', () => {
        it('should return false if site is yahoo', () => {
            chai_1.expect(result.usesGA).to.equal(false);
        });
    });
    describe('Site Protocol', () => {
        it('should return true if the site uses https protocol', () => {
            result.siteProtocol = linkProcessor.getSiteProtocol(protocolData);
            chai_1.expect(result.siteProtocol).to.equal(false);
        });
    });
});
