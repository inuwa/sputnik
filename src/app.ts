import express = require('express');
import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as path from 'path';
import { LinkProcessor } from './controllers/linkProcessor';
import * as csrf from 'csurf';

let app = express();
let linkProcessor = new LinkProcessor();
app.use(express.static(path.join(__dirname, '/../src/stylesheets/')));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
let csrfProtection = csrf({ cookie: true });
app.set('view engine', 'pug');
app.set('views', path.join(__dirname, '/../src/views'));

app.get('/',csrfProtection, (req, res) => {
    res.render('index', { message: 'Sputnik Technical Test', title: 'Sputnik Technical Test', _csrf: (<any>req).csrfToken() });
});

app.post('/', (req, res) => linkProcessor.processLink(req, res));

export default app;
