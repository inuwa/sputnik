"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const linkProcessor_1 = require("../controllers/linkProcessor");
const chai_1 = require("chai");
require("mocha");
describe('Test Questions', () => {
    let linkProcessor = new linkProcessor_1.LinkProcessor();
    let protocolData = { request: { uri: { protocol: 'http:' } } };
    let websiteData = '<!DOCTYPE html>\n<html lang="en">\n\t<head>\n\t<meta name="generator" content="Hugo 0.41-DEV" />\n <title>Yahoo</title></head>\n<body>\n<a class="site-title" href="/">\n<a class="site-title" href="/test">\n</body>\n</html>';
    const result = linkProcessor.getAnswers(websiteData);
    describe('Website Title', () => {
        it('should return title of the website', () => {
            chai_1.expect(result.title).to.equal('Yahoo');
        });
    });
    describe('All Clickable Links', () => {
        it('Number Of Website Links', () => {
            chai_1.expect(result.allLinks).to.equal(2);
        });
    });
    describe('Unique Domains', () => {
        it('Number Of Unique Domains', () => {
            chai_1.expect(result.uniqueDomains).to.equal(2);
        });
    });
    describe('Unique Domains', () => {
        it('Number Of Unique Domains', () => {
            result.siteProtocol = linkProcessor.getSiteProtocol(protocolData);
            chai_1.expect(result.siteProtocol).to.equal(false);
        });
    });
});
